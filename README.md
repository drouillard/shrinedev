[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

Shrine portfolio project uses:

- Node
  - Express
  - Handlebars
- Gulp
- SCSS
  - Bourbon
  - Neat
- Browser Sync

## To run:

```
npm install
gulp
```
