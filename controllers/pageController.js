exports.homePage = (req, res) => {
  const args = {
    isHome: true
  }

  res.render('home', args)
}

exports.projectsPage = (req, res) => {
  const args = {
    title: 'Projects'
  }

  res.render('projects', args)
}

exports.aboutPage = (req, res) => {
  const team = [
    {
      avatar: 'img/douglas_drouillard.jpg',
      name: 'Douglas Drouillard',
      twitter: 'dougdrouillard',
      bio: 'Doug is an experienced web, mobile, and app developer who can fully manage and deliver your project from start to finish. He has a love of all things blockchain and intends to be a major influence on cryptocurrency and technology. Join with him now and let\'s make it happen!'
    },
    {
      avatar: 'img/malissa_drouillard.jpg',
      name: 'Malissa Drouillard',
      twitter: '_maliss',
      bio: 'Malissa is a seasoned full-stack web developer with a knack for problem solving and new technology. She is the reason Doug gets things done! Cryptocurrency and the blockchain fascinate her and she is happy to be a part of this tech world.'
    }
  ]

  const args = {
    title: 'Shrine is on a mission to build your legacy.',
    team
  }

  res.render('about', args)
}

exports.blogPage = (req, res) => {
  const blogPosts = [
    { title: 'Every business is a technology company', date: 'June 15, 2017' },
    { title: 'Two reasons to use a blockchain', date: 'June 18, 2017' },
    { title: 'Introducing Openblock', date: 'May 15, 2017' },
    { title: 'Blog post of variable length', date: 'May 15, 2017' }
  ]
  const args = {
    title: 'Latest from Shrine',
    blogPosts
  }

  res.render('blog', args)
}

exports.contactPage = (req, res) => {
  const args = {
    title: 'Get In Touch'
  }

  res.render('contact', args)
}
