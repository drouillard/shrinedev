const express = require('express')
const router = express.Router()
const pageController = require('../controllers/pageController')

router.get('/', pageController.homePage)
router.get('/projects', pageController.projectsPage)
router.get('/contact', pageController.contactPage)
router.get('/about', pageController.aboutPage)
// router.get('/blog', pageController.blogPage)

module.exports = router
