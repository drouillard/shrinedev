const express = require('express')
const exphbs = require('express-handlebars')
const routes = require('./routes/index')
const helpers = require('./helpers')
const hbs = exphbs.create({ defaultLayout: 'main' })

const app = express()

app.engine('handlebars', hbs.engine)
app.set('view engine', 'handlebars')
app.use(express.static('public'))

app.use((req, res, next) => {
  res.locals.h = helpers;
  next();
});

app.use('/', routes)

app.set('port', process.env.PORT || 8000)
const server = app.listen(app.get('port'), () => {
  console.log(`Express running → PORT ${server.address().port}`)
})

module.exports = app