exports.siteName = `Shrine Development`

exports.menu = [
  // { slug: '/blog', title: 'Blog' },
  { slug: '/about', title: 'About' },
  { slug: '/contact', title: 'Contact' }
]

exports.footer = [
  { link: 'https://twitter.com/dougdrouillard', class: 'twitter', name: '@dougdrouillard' },
  { link: 'https://github.com/drouillard', class: 'github', name: 'Github' },
  { link: 'https://www.linkedin.com/in/drouillard/', class: 'linkedin', name: 'LinkedIn' }
]
